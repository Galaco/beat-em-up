INTERFACE mainmenu
BUTTON 
coordinates=0.25 0.3
textcoordinates=0.1 0.1
text=Single Player
font=default
fontsize=24;
color=255 255 255 255
texture=resource/materials/gui/menu/button_01.png
type=play

BUTTON 
coordinates=0.25 0.5
textcoordinates=0.1 0.1
text=Exit
font=default
fontsize=24;
color=255 255 255 255
texture=resource/materials/gui/menu/button_01.png
type=exit

IMAGE
coordinates=0.2 0.2 
position=behind
texture=resource/materials/gui/menu/underlay.png