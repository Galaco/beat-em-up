#include "prop.h"

Prop::Prop(){
}

Prop::Prop( double x , double y , double z , 
			double rx , double ry , double rz , 
			double sx , double sy , double sz ,
			std::string d , std::string e )
{ 
	char * f = (char*)e.c_str();
	posX = x;
	posY = y;
	posZ = z;
	rotX = rx;
	rotY = ry;
	rotZ = rz;
	scaleX = sx;
	scaleY = sy;
	scaleZ = sz;
	velX = 0;
	velY = 0;
	velZ = 0;
}