#include "globaldata.h"

namespace GlobalData
{
	HandleInput input;
	Renderer renderer;
	AssetLoader resLoader;
	Settings gameSettings;
	Camera camera;
	//Hud GUI;
	FontManager fontmanager;

	unsigned int winWidth , winHeight;
	unsigned int defaultWidth = 960;
	unsigned int defaultHeight = 540;
}