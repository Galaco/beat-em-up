#ifndef INTERFACE_H
#define INTERFACE_H
#include <iostream>
#include <string>


#include "globaldata.h"
#include "gui_button.h"
#include "gui_object.h"

class Interface
{
public:
	Interface();
	Interface( std::string );

	void addButton( double , double , 
					double , double , 
					std::string , sf::Color ,
					sf::Font& , double , std::string , std::string );

	void addItem( double , double , std::string , std::string );


	void draw( sf::RenderWindow* );
			

	~Interface();


	std::string name;
		
protected:

private:
	std::vector<ButtonGUI*> button;
	std::vector<ObjectGUI*> object;

};
#endif