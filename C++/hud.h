#ifndef HUD_H
#define HUD_H
#include "SFML/Graphics.hpp"
#include <iostream>


#include "interface.h"


class Hud
{
public:
	Hud();									// Default constructor

	void draw( sf::RenderWindow* win , std::string );
	void update();


	bool load();



	void addInterface( std::string );

	~Hud();

protected:
	ifstream parser;
	std::stringstream ss;

private:
	
	std::vector<Interface*> interfaces;


	
	double coordinates[8];
	unsigned int colorVals[4];
};
#endif