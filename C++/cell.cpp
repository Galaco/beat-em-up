#include "cell.h"

Cell::Cell(){
	posX = 0;
	posY = 0;
	posY = 0;
	rotX = 0;
	rotY = 0;
	rotZ = 0;
	scaleX = 0;
	scaleY = 0;
	scaleZ = 0;
}

Cell::Cell( double x , double y , double z , 
			double rx , double ry , double rz ,
			double sx , double sy , double sz ){
	posX = x;
	posY = y;
	posZ = z;
	rotX = rx;
	rotY = ry;
	rotZ = rz;
	scaleX = sx;
	scaleY = sy;
	scaleZ = sz;
}


void Cell::addProp( double x , double y , double z , 
					double rx , double ry , double rz,
					double sx , double sy , double sz ,
					std::string mDir , std::string tDir ){

	char * f = (char*)tDir.c_str();
	prop.push_back( new Prop( x+posX, y+posY, z+posZ , rx+rotX , ry+rotY , rz+rotZ , sx+scaleX , sy+posY , sz+posZ , mDir , tDir ) );
	GlobalData::resLoader.loadImage( f , prop[ prop.size() - 1 ] ); 
	GlobalData::resLoader.loadObj( mDir ,prop[ prop.size() - 1 ]->getMesh() );

}



void Cell::draw(){
	for( unsigned int i = 0; i < prop.size(); ++i )
		prop[i]->draw();

}


Cell::~Cell(){
}