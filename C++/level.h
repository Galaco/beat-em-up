#ifndef LEVEL_H
#define LEVEL_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <list>

#include "cell.h"



using namespace std;

class Level
{
public:
	Level();
	bool load();


	void draw();

	~Level();

protected:
	ifstream parser;
	std::stringstream ss;

private:
	std::vector<Cell*> cell;



	int levelNo;
	string type , mDir , tDir , lDir;
	double transformVals[9];
};
#endif