#include "settings.h"


Settings::Settings(){
	width = 960;
	height = 540;
	antialiasing = 0;
	volume = 100;
}

bool Settings::read(){
	std::ifstream f;
	std::string d;
	std::string holder;
	f.open( "resource/cfg/game.cfg" );
	if( ! ( f.is_open() ) )
	{
		std::cout << "cfg not found. Using default settings" << std::endl;
		return false;
	}
	while( f >> holder )
	{
		if( ( holder == "#" ) || ( holder == "g" ) )
			getline(f , d , '\n' );
		else
		if( holder == "width" )
			f >> width;
		else
		if( holder == "height" )
			f >> height;
		else
		if( holder == "antialiasing" )
			f >> antialiasing;
		else
		if( holder == "fullscreen" ){
			int a;
			f >> a;
			if( a == 1 ) fullscreen = true; else fullscreen = false;
		} else
		if( holder == "volume" )
			f >> volume;
		else
		if( holder == "verticalsync" ){
			int a;
			f >> a;
			if( a == 1 ) verticalsync = true; else verticalsync = false;
		}
	}
	f.close();
	return true;
}

bool write(){
	return true;
}



int Settings::getWidth(){ return width; }
int Settings::getHeight(){ return height; }
int Settings::getVolume(){ return volume; }
int Settings::getAA() { return antialiasing; }
bool Settings::getVsync(){ return verticalsync; }
bool Settings::getFullscreen(){ return fullscreen; }


Settings::~Settings(){
}