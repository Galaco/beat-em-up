#ifndef GUIBUTTON_H
#define GUIBUTTON_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>

#include "globaldata.h"


class ButtonGUI
{

public:
	ButtonGUI();
	ButtonGUI( double, double, double, double, std::string, sf::Color, sf::Font& , double , std::string , std::string );

	void draw( sf::RenderWindow* win );
	void update();
	void press();



	void setText( std::string );
	void setTextPosition( double , double );
	void setPosition( double , double );

	double getPosX();
	double getPosY();


	
	~ButtonGUI();


private:
	sf::Image i_btn;
	sf::Texture t_btn;
	sf::Sprite s_btn;
	sf::Text s_name;

	std::string btnType;

	double posX , posY;
}; 
#endif