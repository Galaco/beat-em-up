#ifndef CAMERA_H
#define CAMERA_H
#include <SFML/OpenGL.hpp>
#include <iostream>



class Camera
{
public:
	Camera();						// Default constructor
	void zoom( int );				// Zooms the camera in the z direction
	void update( double , double , double );	// Updates camera (basically just its position)
	
	float getZoom();

	double getX(){ return posX; };
	double getY(){ return posY; };
	double getZ(){ return posZ; };

	void setX( double x ){ posX = x; };
	void setY( double y ){ posY = y; };
	void setZ( double z ){ posZ = z; };

	~Camera();

protected:


private:
	static const int CAMERAMINZOOM = -150;
	static const int CAMERAMAXZOOM = -380;
	static const int CAMERAZDIST = +250;
	static const int YOFFSET = 50;
	double posX , posY , posZ;
};
#endif