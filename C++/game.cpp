#include "game.h"


Game::Game() //constructor
{
}

Game::Game( sf::RenderWindow* app ) : win( app ) //initialise window
{
	cout<<"OpenGL version: "<<glGetString(GL_VERSION)<<endl;				// Display openGL version
	win->setFramerateLimit( FRAMERATE );									// Set framerate
}

void Game::init()
{
	GUI.load();
	level.load();
}

void Game::draw()
{
	level.draw();
	win->pushGLStates();
	GUI.draw( win , "mainmenu" );
	win->popGLStates();
}

void Game::getInput()
{
	GlobalData::input.receiveInput( win );
}
	

void Game::update()
{		
	GlobalData::camera.update( 0 , 100 , 400 );

	t_Clock = clock.getElapsedTime();
	if( t_Clock.asMilliseconds()/10 > 1.0F / FRAMERATE )
	{	
		








		clock.restart();
	} 
}


void Game::play()
{
	//the game loop
	draw();								//the offscreen buffer
	getInput();							//from the user to check for exit
	update();							//integrate user input and any game movement
	win->display();						//display the rendered frame
}

Game::~Game()
{
}