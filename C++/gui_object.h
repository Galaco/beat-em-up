#ifndef GUIOBJECT_H
#define GUIOBJECT_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>

#include "globaldata.h"


class ObjectGUI
{

public:
	ObjectGUI();
	ObjectGUI( double, double, std::string , std::string );

	void draw( sf::RenderWindow* win );
	void update();


	void setPosition( double , double );

	double getPosY(){ return posY; };
	double getPosX(){ return posX; };
	std::string getDrawPriority(){ return drawPriority; };


	
	~ObjectGUI();


private:
	sf::Image img;
	sf::Texture tex;
	sf::Sprite sprt;

	double posX , posY;
	std::string drawPriority;
}; 
#endif