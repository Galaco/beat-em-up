#ifndef HANDLEINPUT_H
#define HANDLEINPUT_H
#include <SFML/Graphics.hpp>
#include <iostream>


class HandleInput
{
public:
	HandleInput();

	void receiveInput( sf::RenderWindow* );

	std::string getInput(){ return keycode; }

	~HandleInput();

	
protected:
	void doProcess( sf::RenderWindow* );

private:
	std::string keycode;
};
#endif