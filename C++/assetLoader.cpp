#include "assetLoader.h"


AssetLoader::AssetLoader(){
}

bool AssetLoader::loadObj( std::string filename , Mesh& mesh ){
	vector<GLfloat> localVertices , localNormals , localUVs, storedUVs, storedNormals;
	std::ifstream objFile;
	std::string type , d;
	GLfloat coord = 0;
	GLushort val = 0;
	objFile.open( filename );
	if( ! ( objFile.is_open() ) )
	{
		std::cout << "Model: could not load " << filename << std::endl;
		return false;
	}
	while( objFile >> type )
	{
		if( ( type == "#" ) || ( type == "g" ) || ( type == "s" ) || ( type == "mtllib" ) )
			getline(objFile , d , '\n' );
		else
		if( type == "v" )
			for( int i=0 ; i < 3 ; ++i ){
				objFile >> coord;
				localVertices.push_back( coord );
			}
		else
		if( type == "f" )
			for( int i=0 ; i < 3 ; ++i ){
				getline(objFile , d , '/' );
				val = atoi( d.c_str() );	
				mesh.addFace( val - 1 );
				getline(objFile , d , '/' );
				val = atoi( d.c_str() );	
				storedUVs.push_back( (float)(val - 1) );
				getline(objFile , d , ' ' );
				val = atoi( d.c_str() );
				storedNormals.push_back( (float)(val - 1) );
			}
		else
		if( type == "vn" )
			for( int i=0 ; i < 3 ; ++i ){
				objFile >> coord;
				localNormals.push_back( coord );
			}
		else
		if( type == "vt" )
			for( int i=0 ; i < 3 ; ++i ){
			objFile >> coord;
			localUVs.push_back( coord );
		}
	}

	for( unsigned int i=0; i < mesh.getFaces().size(); ++i )
	{
		mesh.addVertex( localVertices[ mesh.getFaces()[i]*3 ] );	
		mesh.addVertex( localVertices[ mesh.getFaces()[i]*3 + 1] );
		mesh.addVertex( localVertices[ mesh.getFaces()[i]*3 + 2] );

		mesh.addNormal( localNormals[ (unsigned int)storedNormals[i]*3 ] );	
		mesh.addNormal( localNormals[ (unsigned int)storedNormals[i]*3 + 1] );
		mesh.addNormal( localNormals[ (unsigned int)storedNormals[i]*3 + 2] );

		mesh.addUV( localUVs[ (unsigned int)storedUVs[i]*3  ] );	
		mesh.addUV(1 - localUVs[ (unsigned int)storedUVs[i]*3 + 1] );
		mesh.addUV( localUVs[ (unsigned int)storedUVs[i]*3 + 2] );
	}
	
	objFile.close();

	return true;
}

GLuint AssetLoader::loadImage( const char * filename , Renderable* const& r ){
	string ext;
	
	const char *dot = strrchr(filename, '.');	// Gets fix extension (.xxx)
	ext = dot + 1;


	if( ext == "raw" || ext == "Raw" || ext == "RAW" )	// handle .raw
		r->setTextureID( loadRaw( filename , r->getTextureID() ) );
	else
	if( ( ext == "png" || ext == "Png" || ext == "PNG" ) || // handle .png
		( ext == "jpg" || ext == "Jpg" || ext == "JPG" ) || // handle .jpg
		( ext == "tga" || ext == "Tga" || ext == "TGA" ) )  // handle .tga
		r->setTextureID( loadSFMLImage( filename , r->getTextureID() ) );
	
	
	return r->getTextureID();
}

GLuint AssetLoader::loadRaw( const char * filename , GLuint texture ){	
	char *data;
	int size = 3;
	int width , height;

    std::fstream file( filename , std::ios::binary | std::ios::in | std::ios::ate );
	if ( !file.good() )
	{
		std::cout << "Texture: could not load " << filename << std::endl;
		return 0;
	}
	float s = (float)file.tellg();
	file.seekg( 0 );
	int  i = 1;
	while ( i <=2048 )
	{
		i = i * 2;
		if( (s/4) == i*i ) { size = 4; i=2049; } 
	}
	if(size==4)
		width = (int)sqrt( (s/4) ); 
	else 
		width = (int)sqrt( (s/3) );

	height = width;

	// allocate buffer
	data = static_cast<char*>( malloc( width * height * size ) );
	// read texture data
	file.read( data, width * height * size );
	file.close();

    // allocate a texture name
    glGenTextures( 1 , &texture );

    // select current texture
    glBindTexture( GL_TEXTURE_2D , texture );

	// build our texture MIP maps
	if ( size == 4 )
	{
		gluBuild2DMipmaps( GL_TEXTURE_2D , 4 , width , height, GL_RGBA16, GL_UNSIGNED_BYTE, data );
		glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA16, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data );
		glTexEnvi( GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_REPLACE );
	} else {
		gluBuild2DMipmaps( GL_TEXTURE_2D , 3 , width , height, GL_RGB, GL_UNSIGNED_BYTE, data );
		glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data );
		glTexEnvi( GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE );
	}
	glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_MIN_FILTER , GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_MAG_FILTER , GL_NEAREST);

	free(data);
    return texture;
}

GLuint AssetLoader::loadSFMLImage( const char * filename , GLuint texture ){
	if(!image.loadFromFile( filename ) )
		cout << "Texture: Could not load " << filename << endl;
	int width = image.getSize().x;
	int height = image.getSize().y;

	// allocate a texture name
    glGenTextures( 1 , &texture );
	
    // select current texture
    glBindTexture( GL_TEXTURE_2D , texture );

	// build our texture MIP maps
	//gluBuild2DMipmaps( GL_TEXTURE_2D , 3 , width , height, GL_RGBA16, GL_UNSIGNED_BYTE, image.GetPixelsPtr() );
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8, width , height, 0, GL_RGBA
		, GL_UNSIGNED_BYTE, image.getPixelsPtr() );
	glTexEnvi( GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE );



	glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_MIN_FILTER , GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_MAG_FILTER , GL_LINEAR);

    return texture;
}

AssetLoader::~AssetLoader(){
}