#include "interface.h"

Interface::Interface(){
}

Interface::Interface( std::string d ){
	name = d;
}



void Interface::draw( sf::RenderWindow* win ){
	for( unsigned int i=0; i< object.size(); ++i)
		if( object[i]->getDrawPriority() == "behind" )
			object[i]->draw( win );

	for( unsigned int i=0; i< button.size(); ++i)
		button[i]->draw( win );

	for( unsigned int i=0; i< object.size(); ++i)
		if( object[i]->getDrawPriority() != "behind" )
			object[i]->draw( win );
}





void Interface::addButton( double x , double y , double tx, double ty, 
						   std::string text, sf::Color col, sf::Font& font ,
						   double fontSize , std::string iLoc , std::string type ){
	button.push_back( new ButtonGUI( x, y, tx, ty, text, col, font , fontSize , iLoc , type ) );
}


void Interface::addItem( double x , double y , std::string type , std::string iLoc ){
	object.push_back( new ObjectGUI( x , y , type , iLoc ) );
}


Interface::~Interface(){
}