#include "gui_button.h"


ButtonGUI::ButtonGUI(){
	s_name.setString( " " );
}


ButtonGUI::ButtonGUI( double x , double y , double tx , double ty , std::string d , sf::Color col , 
					  sf::Font& f , double fontSize , std::string iLoc , std::string type ){
	posX = GlobalData::winWidth*x;
	posY = GlobalData::winHeight*y;

	s_name.setFont( f );
	s_name.setString( d );
	s_name.setColor( col );
	s_name.setCharacterSize( static_cast<unsigned int>(fontSize * (GlobalData::winWidth / GlobalData::defaultWidth) ) );

	i_btn.loadFromFile( iLoc );
	i_btn.createMaskFromColor( sf::Color ( 0 , 255 , 0 ) );
	t_btn.loadFromImage( i_btn );
	s_btn.setTexture( t_btn );


	s_btn.setPosition( static_cast<float>(posX) , static_cast<float>(posY) );
	s_btn.setScale( static_cast<float>(GlobalData::winWidth / GlobalData::defaultWidth) , 
		            static_cast<float>(GlobalData::winHeight / GlobalData::defaultHeight) );
	setTextPosition( tx , ty );


	btnType = type;
	
}




void ButtonGUI::draw( sf::RenderWindow* win )
{	
	win->draw( s_btn );
	win->draw( s_name );
}





void ButtonGUI::update()
{

}





void ButtonGUI::setPosition( double x , double y )
{
	posX = x;
	posY = y;
	s_btn.setPosition( static_cast<float>(x) , static_cast<float>(y) );
	s_name.setPosition( static_cast<float>(posX + i_btn.getSize().x*x) , static_cast<float>(posY + i_btn.getSize().y*y) );
}

void ButtonGUI::setTextPosition( double x , double y )
{
	s_name.setPosition( static_cast<float>(posX + i_btn.getSize().x*x) , static_cast<float>(posY + i_btn.getSize().y*y) );
}





ButtonGUI::~ButtonGUI(){

}





double ButtonGUI::getPosY(){ return posY; }
double ButtonGUI::getPosX(){ return posX; }
void ButtonGUI::setText( std::string d ){ s_name.setString( d ); }