#ifndef GAME_H
#define GAME_H
#include <SFML/Graphics.hpp>
#include <iostream>


#include "level.h"
#include "hud.h"
#include "globaldata.h"



#include "prop.h"


using namespace std;

const int FRAMERATE=60;				// the number of updates per second

class Game
{
public:
	Game( sf::RenderWindow* );
	Game();							// default constructor - good practice to always include
	
	void init();					// access resource
	void draw();					// draws the game frame
	void getInput();				// gets the input from user
	void update();					// takes care of any updating
	void play();

	~Game();						// destructor

protected:
	//required for all
	sf::RenderWindow* win;			// a pointer to the window to draw to
	sf::Clock clock;				// for timing the movement
	sf::Time t_Clock;
	Level level;

	Hud GUI;


private:
	//game specifics



};
#endif