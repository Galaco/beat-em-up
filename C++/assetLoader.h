//*******************************************************************
// All encompassing class for loading openGL assets
// Support for: .obj objects
// Support for: .raw textures
//*******************************************************************

#ifndef ASSETLOADER_H
#define ASSETLOADER_H
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <cmath>

#include "renderable.h"
#include "mesh.h"

using namespace std;


class AssetLoader {
public:
	AssetLoader();

	bool loadObj( std::string , Mesh& ); // load .obj
	GLuint loadImage( const char * , Renderable* const& );	// laods and creates a texture

	void deleteTex();
	~AssetLoader();

protected:

private:
	GLuint loadSFMLImage( const char * , GLuint );	// loads and generates a texture from sf::Image supported formats
	GLuint loadRaw( const char * , GLuint );			// loads and generates a texture directly from the .raw format


	sf::Image image;
};
#endif