#include "renderable.h"


Renderable::Renderable(){
	posX = 0;
	posY = 0;
	posZ = 0;
	rotX = 0;
	rotY = 0;
	rotZ = 0;
	scaleX = 0;
	scaleY = 0;
	scaleZ = 0;
}

void Renderable::update() {

}

void Renderable::draw() {						
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glDepthMask( false );
		glEnable( GL_TEXTURE_2D );
		glVertexPointer( 3 , GL_FLOAT , 0 , &mesh.getVertices()[0] );
		glNormalPointer( GL_FLOAT, 0 , &mesh.getNormals()[0] );
		glTexCoordPointer( 3, GL_FLOAT, 0 , &mesh.getUVs()[0] );

		glPushMatrix();
		glTranslatef( static_cast<GLfloat>(posX) , static_cast<GLfloat>(posY) , static_cast<GLfloat>(posZ) );
		glScalef( static_cast<GLfloat>(scaleX) , static_cast<GLfloat>(scaleY) , static_cast<GLfloat>(scaleZ) );
		glRotatef( static_cast<GLfloat>(rotX) , 1 , 0 , 0 );
		glRotatef( static_cast<GLfloat>(rotY) , 0 , 1 , 0 );
		glRotatef( static_cast<GLfloat>(rotZ) , 0 , 0 , 1 );
		glBindTexture( GL_TEXTURE_2D , texture );
		glDrawArrays(GL_TRIANGLES, 0 , (GLsizei)mesh.getVertices().size()/3 );
		glPopMatrix();

		glDisable( GL_TEXTURE_2D );
		glDepthMask( true );
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);	
}


Renderable::~Renderable(){
	glDeleteTextures( 1 ,  &texture );
}