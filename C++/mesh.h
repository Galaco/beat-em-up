#ifndef MESH_H
#define MESH_H
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <vector>

#include "bone.h"

class Mesh
{
public:
	Mesh();


	void addBone();




	std::vector<GLfloat>& getVertices(){ return vertices; }
	std::vector<GLfloat>& getNormals(){ return normals; }
	std::vector<GLfloat>& getUVs(){ return UVs; }
	std::vector<GLushort>& getFaces(){ return faces; }


	void addFace( GLushort d ){ faces.push_back( d ); }
	void addVertex( GLfloat d ){ vertices.push_back( d ); }
	void addUV( GLfloat d ){ UVs.push_back( d ); }
	void addNormal( GLfloat d ){ normals.push_back( d ); }
	~Mesh();

		
protected:
	std::vector<GLfloat> vertices;
	std::vector<GLushort> faces;
	std::vector<GLfloat> normals;
	std::vector<GLfloat> UVs;

	std::vector<GLfloat> Bones;


private:
};
#endif