#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include "game.h"

#include "globaldata.h"

using namespace std;


int main()
{
	GlobalData::gameSettings.read();
	GlobalData::winWidth = GlobalData::gameSettings.getWidth();
	GlobalData::winHeight = GlobalData::gameSettings.getHeight();

	sf::ContextSettings cs;
	cs.majorVersion = 3;
	cs.minorVersion = 2;
	cs.depthBits = 24;
	cs.stencilBits = 8;
	cs.antialiasingLevel = GlobalData::gameSettings.getAA();

	// Create Window
	sf::RenderWindow App( sf::VideoMode( GlobalData::winWidth , GlobalData::winHeight ), "Scrolling Shooter" , sf::Style::Close, cs );
	if( GlobalData::gameSettings.getFullscreen() == true ){
		App.create( sf::VideoMode::getDesktopMode() , "Scrolling Shooter" , sf::Style::Fullscreen, cs );
	}
	
	Game g( &App );
	App.setVerticalSyncEnabled( GlobalData::gameSettings.getVsync() );

	// OpenGL setup	
    glClearDepth( 1.f );							// Set color and depth clear value
    glClearColor( 0.f , 0.f , 0.f , 0.f );
    glEnable( GL_DEPTH_TEST );						// Enable Z-buffer read and write
	glDepthFunc( GL_LEQUAL );
    glDepthMask(GL_TRUE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable( GL_LIGHTING );						// Enable Lighting 
    glMatrixMode( GL_PROJECTION );					// Setup a perspective projection
	glDisable(GL_COLOR_MATERIAL);
    glLoadIdentity();
    gluPerspective( 90.f , GlobalData::winWidth/GlobalData::winHeight , 1.f , 1536.f );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	g.init();					// Initialise game - loads resources

	// Start the game loop
    while ( App.isOpen() )
    {	
		App.setActive();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		g.play(); 											
    }

	cout << endl;


    return EXIT_SUCCESS;
}