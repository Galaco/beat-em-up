#include "gui_object.h"


ObjectGUI::ObjectGUI(){
}


ObjectGUI::ObjectGUI( double x , double y, std::string position , std::string iLoc ){
	posX = GlobalData::winWidth*x;
	posY = GlobalData::winHeight*y;

	drawPriority = position;


	img.loadFromFile( iLoc );
	img.createMaskFromColor( sf::Color ( 0 , 255 , 0 ) );
	tex.loadFromImage( img );
	sprt.setTexture( tex );
	sprt.setScale( static_cast<float>(GlobalData::winWidth / GlobalData::defaultWidth) , 
		           static_cast<float>(GlobalData::winHeight / GlobalData::defaultHeight) );

	sprt.setPosition( static_cast<float>(posX) , static_cast<float>(posY) );
	
}




void ObjectGUI::draw( sf::RenderWindow* win )
{	
	win->draw( sprt );
}





void ObjectGUI::update()
{

}





void ObjectGUI::setPosition( double x , double y )
{
	posX = x;
	posY = y;
	sprt.setPosition( static_cast<float>(x) , static_cast<float>(y) );
}





ObjectGUI::~ObjectGUI(){

}

