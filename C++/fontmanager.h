#ifndef FONTMANAGER_H
#define FONTMANAGER_H
#include <iostream>
#include "SFML/Graphics.hpp"


class FontManager
{
	static sf::Font defaultFont;


public:
	FontManager();

	sf::Font& getFont( std::string );


	~FontManager();



		
protected:

private:
	

};
#endif