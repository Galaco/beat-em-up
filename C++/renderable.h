#ifndef RENDERABLE_H
#define RENDERABLE_H
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <fstream>
#include <string>

#include "mesh.h"
#include "AABB.h"


class Renderable
{
public:
	Renderable();
	void update();

	void draw();




	Mesh& getMesh(){ return mesh; }
	AABB& getBB(){ return bb; }
	GLuint& getTextureID(){ return texture; }


	void setTextureID(GLuint t){ texture = t; }

	virtual ~Renderable();

	
protected:
	// Setup model information
	double posX , posY , posZ;
	double velX , velY , velZ;
	double scaleX , scaleY , scaleZ;
	double rotX , rotY , rotZ;

	GLuint texture;
	
	Mesh mesh;
	AABB bb;


private:
};
#endif