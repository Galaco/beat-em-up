#include "level.h"

Level::Level(){
	levelNo = 1;
}


bool Level::load(){
	ss << levelNo;
	lDir = "resource/maps/level" + ss.str() + ".map";
	parser.open( lDir );
	if ( !parser.is_open() || parser.bad() ) 
		return false;	
	string holder;

	while ( parser >> type )
	{
		if( type == "#" ) getline(parser , holder , '\n' );
		if( type == "CELL" ) {
			for( int i=0; i<9; ++i )
				parser >> transformVals[i];
			
			cell.push_back( new Cell( transformVals[0] , transformVals[1] , transformVals[2] , 
									  transformVals[3] , transformVals[4] , transformVals[5] ,
									  transformVals[6] , transformVals[7] , transformVals[8] ) ); 
		} else
		if( type == "PROP" ) {
			for( int i=0; i<9; ++i )
				parser >> transformVals[i];
			parser >> mDir;
			parser >> tDir;
			cell[cell.size()-1]->addProp( transformVals[0] , transformVals[1] , transformVals[2] , 
										  transformVals[3] , transformVals[4] , transformVals[5] ,
										  transformVals[6] , transformVals[7] , transformVals[8] , 
										  mDir , tDir );
		}



	}
	parser.close();

	return true;
}


void Level::draw(){
	for( unsigned int i=0; i < cell.size(); ++i )
	{
		cell[i]->draw();
	}
}






Level::~Level(){
}