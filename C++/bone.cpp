#ifndef MESH_H
#define MESH_H
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <fstream>
#include <vector>


class Mesh
{
public:
	Mesh();
	void draw();				// draws the object


	~Mesh();

		
protected:
	std::vector<GLfloat> vertices;
	std::vector<GLushort> indices;
	std::vector<GLfloat> normals;
	std::vector<GLfloat> texcoords;

private:
};
#endif