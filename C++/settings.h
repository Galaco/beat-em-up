#ifndef SETTINGS_H
#define SETTINGS_H
#include <iostream>
#include <string>
#include <fstream>

class Settings {
public:
	Settings();
	bool read();
	bool write();

	int getWidth();
	int getHeight();
	int getVolume();
	int getAA();
	bool getFullscreen();
	bool getVsync();

	~Settings();


protected:


private:
	int width , height;
	int volume , antialiasing;
	bool verticalsync , fullscreen;
};
#endif