#ifndef CELL_H
#define CELL_H
#include <iostream>
#include <list>


#include "globaldata.h"
#include "prop.h"

class Cell
{
public:
	Cell();
	Cell( double , double , double , 
		  double , double , double ,
		  double , double , double );

	void addProp( double , double , double , 
				  double , double , double ,
				  double , double , double ,
				  std::string , std::string );



	void draw();
			

	~Cell();

		
protected:

private:
	std::vector<Prop*> prop;
	double posX , posY , posZ , rotX , rotY , rotZ , scaleX , scaleY , scaleZ;

};
#endif