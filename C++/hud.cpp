#include "hud.h"


Hud::Hud(){

}



bool Hud::load(){
	parser.open( "resource/resource/gui_layout.res" );
	if ( !parser.is_open() || parser.bad() ) 
		return false;
	string type , text , font , holder , img;
	double fontSize;

	while ( parser >> type )
	{
		if( type == "#" ) 
			getline(parser , holder , '\n' );
		else
		if( type == "INTERFACE" ){
			parser >> holder;
			interfaces.push_back( new Interface( holder ) ); 
		} 
		else
		if( type == "BUTTON" ) {
			getline(parser , holder , '=' );
			parser >> coordinates[0];
			parser >> coordinates[1];
			getline(parser , holder , '=' );
			parser >> coordinates[2];
			parser >> coordinates[3];

			getline(parser , holder , '=' );
			getline(parser , text , '\n' );
			getline(parser , holder , '=' );
			getline(parser , font , '\n' );
			getline(parser , holder , '=' );
			parser >> fontSize;
			
			getline(parser , holder , '=' );
			parser >> colorVals[0];
			parser >> colorVals[1];
			parser >> colorVals[2];
			parser >> colorVals[3];

			sf::Color color( colorVals[0] , colorVals[1] , colorVals[2] , colorVals[3] ) ;

			getline(parser , holder , '=' );
			getline(parser , img , '\n' );

			getline(parser , holder , '=' );
			getline(parser , type , '\n' );


			interfaces[interfaces.size()-1]->addButton( coordinates[0] , coordinates[1] , coordinates[2] , 
													    coordinates[3] , text , color , 
														GlobalData::fontmanager.getFont( font ) , fontSize , img , type );
		} else
		if( type == "IMAGE" ) {
			getline(parser , holder , '=' );
			parser >> coordinates[0];
			parser >> coordinates[1];

			getline(parser , holder , '=' );
			getline(parser , type , '\n' );
			getline(parser , holder , '=' );
			getline(parser , img , '\n' );

			interfaces[interfaces.size()-1]->addItem( coordinates[0] , coordinates[1] , type , img );
		}



	}




	parser.close();

	return true;
}







void Hud::draw( sf::RenderWindow* win , std::string s ){
	for( unsigned int i=0; i<interfaces.size(); ++i )
		if( s==interfaces[i]->name )
			interfaces[i]->draw( win );

}


void Hud::update(){

}
	

Hud::~Hud(){
}