#ifndef GLOBALDATA_H
#define GLOBALDATA_H
#include "SFML/Graphics.hpp"

#include "handleinput.h"
#include "renderer.h"
#include "assetLoader.h"
#include "settings.h"
#include "camera.h"
#include "fontmanager.h"


namespace GlobalData
{
	extern HandleInput input;
	extern Renderer renderer;
	extern AssetLoader resLoader;
	extern Settings gameSettings;
	extern Camera camera;
	extern FontManager fontmanager;	

	extern unsigned int winWidth, winHeight , defaultWidth, defaultHeight;;
};
#endif GLOBALDATA_H