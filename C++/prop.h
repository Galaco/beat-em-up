#ifndef PROP_H
#define PROP_H

#include "renderable.h"

class Prop : public Renderable
{
public:
	Prop();
	Prop( double , double , double , double , double , double , double , double , double , std::string, std::string );		// our constructor. arguments are x,y
	
protected:


private:

};
#endif