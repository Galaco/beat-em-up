#include "camera.h"


Camera::Camera(){
	posX = 0;
	posY = 0;
	posZ = 0;
}

void Camera::zoom( int d ){
	posZ += d;
	//if ( ( posZ > CAMERAMINZOOM ) || ( posZ < CAMERAMAXZOOM ) )			// Check bounds for min and max camera distance
	//{
	//	posZ -= d;
	//} 
}

void Camera::update( double x , double y , double z ){
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glTranslatef( static_cast<GLfloat>(-x) , static_cast<GLfloat>(-y - YOFFSET) , static_cast<GLfloat>(-z + posZ) );							// Translate the camera in the X and Y direction. 
}

Camera::~Camera(){
}